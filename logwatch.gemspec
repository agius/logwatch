# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'logwatch/version'

Gem::Specification.new do |spec|
  spec.name          = "logwatch"
  spec.version       = Logwatch::VERSION
  spec.authors       = ["agius"]
  spec.email         = ["andrew@atevans.com"]

  spec.summary       = %q{Listens to logs from STDIN, prints statistics. For a tech interview.}
  spec.description   = %q{Listens to logs from STDIN, prints statistics. For a tech interview.}
  spec.homepage      = "https://bitbucket.org/agius/logwatch"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # https://whatisthor.com
  spec.add_runtime_dependency 'thor', '~> 0.19'

  spec.add_runtime_dependency 'curses', "~> 1.2.2"
  
  # https://github.com/tcurdt/http-log-parser
  spec.add_runtime_dependency 'http-log-parser', '~> 0.0'
  
  spec.add_runtime_dependency 'descriptive_statistics', '~> 2.5'

  spec.add_runtime_dependency 'tty-table', '~> 0.8.0'
  spec.add_runtime_dependency 'pastel', '~> 0.7.1'
  spec.add_runtime_dependency 'verse', '~> 0.5.0'

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "awesome_print", "~> 1.8.0"
  spec.add_development_dependency "timecop", "~> 0.9.0"
  spec.add_development_dependency "faker", "~> 1.7.3"
end
