# Logwatch: HTTP log monitoring console program

Create a simple console program that monitors HTTP traffic on your machine:

* Consume an actively written-to [w3c-formatted HTTP access log](https://en.wikipedia.org/wiki/Common_Log_Format)
* Every 10s, display in the console the sections of the web site with the most hits (a section is defined as being what's before the second '/' in a URL. i.e. the section for "http://my.site.com/pages/create' is "http://my.site.com/pages"), as well as interesting summary statistics on the traffic as a whole. 
* Make sure a user can keep the console app running and monitor traffic on their machine 
* Whenever total traffic for the past 2 minutes exceeds a certain number on average, add a message saying that “High traffic generated an alert - hits = {value}, triggered at {time}” 
* Whenever the total traffic drops again below that value on average for the past 2 minutes, add another message detailing when the alert recovered 
* Make sure all messages showing when alerting thresholds are crossed remain visible on the page for historical reasons. 
* Write a test for the alerting logic 
* Explain how you’d improve on this application design

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'logwatch'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install logwatch

## Usage

`logwatch help`

* displays info about available commands and options

`logwatch listen <FILENAME>`

* listens to w3c-formatted HTTP log lines from \<FILENAME\>
* every 10 seconds, prints the top few sections and some fun stats
* prints traffic alert whenever rolling 2min traffic exceeds threshold
* prints another alert when traffic drops back below threshold

`logwatch simulate`

* prints fakey data in the log format to STDOUT
* useful for redirecting to a file you're `listen`ing to

`logwatch fixtures`

* prints fakey data to `spec/fixtures/sample.log` for use in specs
* prints the data structures to `spec/fixtures/stats_stubs.yml` for comparison in specs

## Challenge Notes

**How long did this take?** Ummm, a couple hours ish. Check the git logs for detailed info.

**What did you learn?** Never used [curses](https://github.com/ruby/curses) before! Neat to make a screen-based terminal app. Also, [tty-gems](https://piotrmurach.github.io/tty/gems/) is pretty baller.

**Choices Made:** It wasn't clear how the traffic alert should work - stop every two minutes and see if there's an overflow? Or alert as soon as we pass the threshold based on a rolling window? I picked the latter, because that's what I'd want in an alerting system.

It wasn't totally clear to me at first this should be a GUI-ish application. "Display in the console" sounds like "print to STDOUT," but "make sure messages stay on-screen" doesn't make sense in that context. "User can keep the console app running" sounds like a terminal app, but maybe that just means a daemon?

Anyway, this might actually have been easier for me as a daemon with a server and web-based GUI, or as an Electron app. But hey, good to learn some VT100 history with Curses & co.

**What would you improve?** Don't feel like my testing on this is super great. Mostly because writing the test data fixtures manually is a ton of effort, but when I automated it that means I don't exactly know what to assert about the calculations :joy_cat:

The `expire_old!` pattern could be problematic. It's kinda like stop-the-world garbage collection, and we might eat all memory between purges. One alternative would be to delete older records on every `append` , but the performance there is less predictable than "this might pause every N minutes." Another would be a fixed cap on number of messages retained, but then we have no idea how much time our stats represent.

If I were building this out more, I'd probably take inspiration from [fluentd](https://github.com/fluent/fluentd/tree/master/lib/fluent), which is also in Ruby.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/logwatch. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

