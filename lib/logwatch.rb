require "logwatch/version"

module Logwatch
  # Your code goes here...
end

require "logwatch/log_line"
require "logwatch/stats"
require "logwatch/window"
require "logwatch/runner"
require "logwatch/cli"