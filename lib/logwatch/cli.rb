require 'thor'
require 'io/console'
require "logger"

module Logwatch
  class CLI < Thor

    desc "listen FILE", "Listen to logs in FILE, report stats in pty"
    option :threshold,
      default: 1000,
      type: :numeric,
      desc: "alert if > X requests in last --period"

    option :period,
      default: 2 * 60,
      type: :numeric,
      desc: "alert if > --threshold requests in last X seconds"

    option :stats_interval,
      default: 10,
      type: :numeric,
      desc: "update stats every X seconds"

    option :retention,
      default: 30 * 60,
      type: :numeric,
      desc: "seconds of data to retain; older data will be expired"

    def listen(filename)
      file = File.open(filename, 'r')
      @runner = Runner.new(file, options)
      @runner.run_event_loop
    rescue Interrupt
      # close
    ensure
      @runner.teardown! if @runner
      puts "Thank you for using logwatch"
    end

    desc "simulate", "Pipes simulation data to STDOUT"
    def simulate
      require "logwatch/test"

      randomizer = Random.new(26851)
      loop do
        fb = Test::FixtureBuilder.new(randomizer: randomizer, spec_time: Time.now, time_range: 1)
        randomizer.rand(1000).times do
          puts fb.gen_sample
        end
        sleep(randomizer.rand(5))
      end
    rescue Interrupt
      # close
    end

    desc "fixtures", "Generates fixtures for specs"
    def fixtures
      require "logwatch/test"

      fb = Test::FixtureBuilder.new
      fb.build_samples
      fb.write_logs!
      fb.write_structs!
    end

  end
end
