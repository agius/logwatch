module Logwatch
  module Test
    PROPS = [:ip, :slug, :user, :datetime, :http_method, :section, :url, :status, :bytecount]
    ExampleLine = Struct.new(*PROPS) do

      def <=>(other)
        other.datetime <=> self.datetime
      end

      def to_s
        "
          #{ip} 
          #{slug}
          #{user}
          [#{datetime.strftime('%d/%b/%Y:%H:%M:%S %z')}]
          \"#{http_method}
          #{url}
          HTTP/1.0\"
          #{status}
          #{bytecount}
        ".gsub(/\s+/, ' ').strip
      end
    end
  end
end