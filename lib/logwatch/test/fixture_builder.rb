require "faker"

module Logwatch
  module Test
    class FixtureBuilder

      attr_reader :spec_time, :randomizer, :http_methods, :response_codes, :sections, :time_range

      def initialize(options = {})
        @spec_time = options.fetch(:spec_time, Time.local(2017, 6, 23, 14, 4, 0))
        @time_range = options.fetch(:time_range, 60 * 10)
        @randomizer = options.fetch(:randomizer, Random.new(26851))
        @http_methods = options.fetch(:http_methods, ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
        @response_codes = options.fetch(:response_codes, ['200', '301', '404', '500', '504'])
        @sections = options.fetch(:sections, [
          'pages', 'users', 'l33t_h4x', 'bcrypt', 'beef', 'metasploit', 
          'airodump-ng', 'hack_the_planet', 'hacker-manifesto', 'WannaCry',
          'cryptolocker', 'ETERNALBLUE', 'vault7'
        ])
      end

      def rand_time
        rand_time = spec_time - randomizer.rand(time_range)
      end

      def sampled(arr)
        arr[randomizer.rand(arr.length - 1)]
      end

      def gen_sample
        section = sampled(sections)
        ExampleLine.new(
          Faker::Internet.public_ip_v4_address,
          Faker::Internet.slug,
          Faker::Internet.user_name,
          rand_time,
          sampled(http_methods),
          section,
          Faker::Internet.url('leethackers.net', "/#{section}"),
          sampled(response_codes),
          randomizer.rand(700000) 
        )
      end

      def build_samples
        @samples = 5000.times.map {|_i| gen_sample }
      end

      def write_logs!
        File.open('spec/fixtures/sample.log', 'w') do |f|
          @samples.each {|s| f.puts s.to_s }
        end
      end

      def write_structs!
        File.write('spec/fixtures/stats_stubs.yml', @samples.to_yaml)
      end
    end
  end
end