require "logger"

module Logwatch
  class Runner

    attr_reader :file, :stats, :window, :options

    DEFAULT_OPTIONS = {
      threshold: 1000,
      period: 2 * 60,
      stats_interval: 10,
      retention: 30 * 60
    }

    def initialize(file, options = {})
      @file = file
      @stats = Stats.new(logger)
      @window = generate_window! # easier to stub a method in testing
      @options = DEFAULT_OPTIONS.merge(options)
    end

    def run_event_loop
      window.print_msg("
        Welcome to Logwatch. We'll be monitoring logs from: #{file}. 
        Stats will be refreshed every #{options[:stats_interval]} seconds.
        You'll be alerted if there are more than #{options[:threshold]} 
        hits in the last #{options[:period].to_f / 60} minutes.
        Data and stats represent the last #{options[:retention].to_f / 60}
        minutes.
        You can scroll these messages using the arrow keys.
        Press 'q' to quit.
      ".gsub(/\s+/, ' ').strip)

      loop do
        update_stats if should_update_stats?
        expire_old_data if should_expire_old? # more expensive than #update_stats
        update_alert_status
        window.handle_keyboard_input
        read_from_log
      end
    end

    def update_stats
      window.update_top_sections(stats.top_sections)
      window.update_stats(stats.summary)
      @last_update = Time.now
    end

    def should_update_stats?
      @last_update ||= Time.now
      Time.now.to_i - @last_update.to_i >= options[:stats_interval]
    end

    def read_from_log
      line = file.gets
      return if line.nil?

      ll = LogLine.new(line)
      stats.append(ll)

    rescue Exception => e
      # would be nice if the gem gave us a specific error instead of RuntimeError
      if e.message =~ /Line does not match format/
        logger.error "Failed to parse line: #{line}"
      else
        raise e
      end
    end

    def should_expire_old?
      @last_expire ||= Time.now
      Time.now.to_i - @last_expire.to_i >= options[:retention]
    end

    def expire_old_data
      logger.info "Expiring old data"
      stats.expire_old!(options[:retention])
      @last_expire = Time.now
    end

    # Soooo... it's pretty much always going to say hits = 1000 here
    # I'm not sure if the goal of the project was to check every two minutes,
    # or to constantly check against a rolling 2min count. I picked the latter
    # because that makes more sense to me. I want an alert as soon as we pass
    # the threshold, not at some arbitrary later point when we batch-calculate
    def update_alert_status
      hits = recent_hit_count
      if traffic_alert?(hits)
        logger.info "Traffic alert"
        window.print_msg "High traffic generated an alert - hits = #{hits}, triggered at #{Time.now}"
        @is_above_threshold = true
      
      elsif alert_cleared?(hits)
        logger.info "Alert cleared"
        window.print_msg "Traffic alert cleared as of #{Time.now}"
        @is_above_threshold = false
      end
    end

    def recent_hit_count
      @stats.logs_since(options[:period]).count
    end

    def traffic_alert?(hits)
      hits >= options[:threshold] && !@is_above_threshold
    end

    def alert_cleared?(hits)
      hits < options[:threshold] && @is_above_threshold
    end

    def teardown!
      window.teardown if window
    end

    private

    def generate_window!
      Window.new
    end

    def logger
      @logger ||= Logger.new('logwatch.log')
    end

  end
end