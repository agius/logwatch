require 'http_log_parser'
require 'date'
require 'uri'

module Logwatch

  class LogLine

    attr_reader :line, :data, :datetime, :method, :url, :protocol, :section

    def initialize(line)
      @line = line
      @data = parser.parse_line(line)
      @datetime = DateTime.strptime(@data[:datetime], '%d/%b/%Y:%H:%M:%S %z').to_time
      @method, @url, @protocol = @data[:request].split(' ').map(&:strip)
      @section = extract_section(@url)
    end

    # we generally want these in descending order
    def <=>(other)
      other.datetime <=> datetime
    end

    def parser
      self.class.parser
    end

    def status
      data[:status]
    end

    def bytecount
      data[:bytecount]
    end

    def ip
      data[:ip]
    end

    module ClassMethods
      def parser
        @parser ||= HttpLogParser.new
      end
    end

    extend ClassMethods

    private

    def extract_section(uri)
      URI(uri).path.split('/').reject {|s| s.length == 0 }.first
    rescue Exception => e
      uri.split('/').reject {|s| s.length == 0 }.first
    end

  end
end