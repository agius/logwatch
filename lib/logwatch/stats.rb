require 'descriptive_statistics/safe'
require 'set'

module Logwatch
  class Stats

    TWO_MINUTES = 2 * 60
    TEN_MINUTES = 10 * 60

    attr_reader :lines, :sections, :buckets, :statuses, :ips

    def initialize(logger = nil)
      @logger = logger
      init_data!
    end

    def append(line)
      @lines << line
      @sections[line.section] ||= 0
      @sections[line.section] += 1

      @statuses[line.status] ||= 0
      @statuses[line.status] += 1

      @ips << line.ip
      
      ldt = line.datetime
      min = Time.new(ldt.year, ldt.month, ldt.day, ldt.hour, ldt.min)
      @buckets[min] ||= 0
      @buckets[min] += 1
    end

    def logs_since(n = TWO_MINUTES)
      deadline = Time.now - n
      
      @logger.info "Deadline: #{deadline} | lines: #{lines.count}" if @logger
      lines.each_with_object([]) do |line, res| 
        break res if line.datetime < deadline
        res << line
      end
    end

    def expire_old!(n = TEN_MINUTES)
      deadline = Time.now - n
      existing_lines = @lines.dup
      init_data!
      existing_lines.each do |line|
        break if line.datetime < deadline
        append(line)
      end
    end

    def top_sections(n = 10)
      Hash[sections.sort {|a,b| b[1] <=> a[1] }.take(n)]
    end

    def summary
      totals_stats.merge(qpm_stats).merge(response_stats)
    end

    def totals_stats
      Hash[
        "Total Hits" => @lines.count,
        "Unique IPs" => @ips.count
      ]
    end

    def response_stats
      Hash[@statuses.map {|code, count| ["Response #{code}", count] }]
    end

    def qpm_stats
      return {} unless buckets.count > 1
      vals = buckets.values
      Hash[
        "Mean queries / min" => DescriptiveStatistics.mean(vals),
        "Queries / min STDEV" => DescriptiveStatistics.standard_deviation(vals),
        "90th Perc QPM" => DescriptiveStatistics.percentile(90, vals)
      ]
    end

    private

    def init_data!
      @lines = SortedSet.new
      @sections = {}
      @buckets = {}
      @statuses = {}
      @ips = Set.new
    end

  end
end