require "bundler/setup"
require "logwatch"
require "logwatch/test"
require "awesome_print"
require "timecop"
require "yaml"

$spectime = Time.local(2017, 6, 23, 14, 4, 0)
$rand = Random.new(26851)

$fixture_lines = File.readlines(File.join(File.dirname(__FILE__), 'fixtures', 'sample.log'))
$fixture_structs = YAML.load(File.read(File.join(File.dirname(__FILE__), 'fixtures', 'stats_stubs.yml')))

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:each) do
    Timecop.freeze($spectime)
  end

  config.after(:each) do
    Timecop.return
  end
end
