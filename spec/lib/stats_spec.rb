require "spec_helper"
require "yaml"

RSpec.describe Logwatch::Stats do

  let(:sections) { [
    'pages', 'users', 'l33t_h4x', 'bcrypt', 'beef', 'metasploit', 
    'airodump-ng', 'hack_the_planet', 'hacker-manifesto', 'WannaCry',
    'cryptolocker', 'ETERNALBLUE', 'vault7'
  ] }

  let(:lines) do
    $fixture_structs
  end

  let(:instance) do
    described_class.new.tap do |i|
      lines.each {|l| i.append(l) }
    end
  end

  describe '#lines' do
    subject { instance.lines }
    
    it 'keeps lines in order from most recent to oldest' do
      subject.each_cons(2) {|a, b| expect(b.datetime).to be <= a.datetime }
    end
  end

  describe '#top_sections' do
    subject { instance.top_sections }

    it 'defaults to 10' do
      expect(subject.count).to eq(10)
    end

    it 'returns in order' do
      subject.each_cons(2) {|a, b| expect(b[1]).to be <= a[1] }
    end
  end

  describe '#summary_stats' do
    subject { instance.summary }

    it 'returns mean, stdev and 90th percentile queries per minute' do
      expect(subject).to include("Total Hits", "Response 500", "Mean queries / min")
    end
  end

  describe '#logs_since' do
    subject { instance.logs_since.count }

    it 'returns count of requests in the last two minutes' do
      skip 'need to use known data set here, fixtures are too obtuse'
      expect(subject).to eq(976)
    end
  end

  describe '#expire_old!' do
    let(:time_diff) { 5 * 60 }
    let(:time_ago) { Time.now - time_diff }
    subject { instance }

    it 'clears out old data' do
      logz_count = instance.lines.count
      ips = instance.ips.count
      subject.expire_old!(time_diff)
      expect(instance.lines.count).to be < logz_count
      expect(instance.ips.count).to be < ips
    end

    it 'only retains logs later than time_diff' do
      subject.expire_old!(time_diff)
      any_old = subject.lines.any? {|l| l.datetime < time_ago }
      expect(any_old).to eq(false)
    end
  end

end