require "spec_helper"

RSpec.describe Logwatch::Runner do

  let(:file) { double("File") }
  let(:logger) { double("Logger") }
  let(:window) { double("Logwatch::Window") }
  let(:instance) { described_class.new(file) }

  before do
    allow_any_instance_of(described_class).to receive(:logger).and_return(logger)
    allow_any_instance_of(described_class).to receive(:generate_window!).and_return(window)
  end

  describe '#initialize' do
    it 'uses default options' do
      expect(instance.options[:threshold]).to eq(described_class::DEFAULT_OPTIONS[:threshold])
    end
  end

  describe '#update_stats' do
    subject { instance.update_stats }

    it 'tells the window to render stat tables' do
      expect(window).to receive(:update_top_sections)
      expect(window).to receive(:update_stats)
      subject
    end
  end

  describe '#update_alert_status' do

    let(:threshold) { instance.options[:threshold] }

    context 'when hits are above threshold' do
      before do
        allow(instance).to receive(:recent_hit_count).and_return(threshold + 100)
      end

      it 'raises the alert only once' do
        expect(logger).to receive(:info).once
        expect(window).to receive(:print_msg).once
        instance.update_alert_status
        instance.update_alert_status
      end
    end

    context 'when hits go back below threshold' do
      before do
        allow(instance).to receive(:recent_hit_count).and_return(threshold - 100)
        instance.instance_variable_set(:@is_above_threshold, true)
      end

      it 'posts the message only once' do
        expect(logger).to receive(:info).once
        expect(window).to receive(:print_msg).once
        instance.update_alert_status
        instance.update_alert_status
      end
    end

  end

end