require "spec_helper"

RSpec.describe Logwatch::LogLine do

  let(:line) { $fixture_lines.last }
  let(:fixture) { $fixture_structs.last }
  
  let(:instance) { described_class.new(line) }

  describe '#datetime' do
    let(:datetime) { fixture.datetime  }
    subject { instance.datetime }

    it 'parses datetime' do
      expect(subject.to_time).to eq(datetime)
    end
  end

  describe '#section' do
    let(:section) { fixture.section }
    subject { instance.section }

    it 'parses section' do
      expect(subject).to eq(section)
    end
  end
end
